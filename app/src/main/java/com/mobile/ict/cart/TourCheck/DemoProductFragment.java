package com.mobile.ict.cart.TourCheck;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mobile.ict.cart.activity.ProfileActivity;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Product;
import com.mobile.ict.cart.container.ProductType;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.adapter.ProductAdapter;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

/**
 * Created by vish on 21/3/16.
 */
public class DemoProductFragment extends Fragment {
    private ImageView btnClickMe1;
    private TextView btnClickMe2;
    private TextView btnClickMe3;
    View productFragmentView;
    RelativeLayout relativeLayout;
    Drawer result, result1;
    ProductType productType;
    Product product;
    //ArrayList<ProductType> productTypes ;
    Map <String,String> itemsMap = new HashMap<String, String>();
    Map <String,String> stockQuantityMap = new HashMap<String, String>();
    String stockEnabledStatus="false";
    RecyclerView recyclerView;
    DrawerBuilder drawerBuilder, drawerBuilder1;
    Button gone_swipe, gone_swipe2;
    private GridLayoutManager gridLayout;
    ProductAdapter swapAdapter;
    StaggeredGridLayoutManager layoutManager;
    DBHelper dbHelper;
    ImageView ivNoProduct, ivNoData,swipe, swipe2;
    TextView tNoProduct, tNoData,ivProductName;
    ImageView ivImage1,ivImage2,ivImage3,ivImage4;
    TourGuide mTourGuideHandler;
    int l=0;
    ArrayList<Product> cartList;

    public static int selectedProductTypePosition = 0;


    //ArrayList<Product> cartList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        if(!Master.isNetworkAvailable(getActivity()))
        {
            productFragmentView = inflater.inflate(R.layout.no_internet_layout, container, false);


            /*productFragmentView = inflater.inflate(R.layout.fragment_products, container, false);

            dbHelper = new DBHelper(getActivity());

            //getActivity().item.setVisible(true);

            getActivity().setTitle(R.string.title_fragment_product);
            relativeLayout = (RelativeLayout) productFragmentView.findViewById(R.id.relativeLayout);
            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

            recyclerView = (RecyclerView) productFragmentView.findViewById(R.id.rvProduct);
            recyclerView.setHasFixedSize(true);


            recyclerView.setLayoutManager(layoutManager);

            Master.productList = new ArrayList<>();
            Master.productList.add(0,new Product("name", 10, 0, 100, "true", "null", "null", "142"));
            ProductAdapter rcAdapter = new ProductAdapter(getActivity());
            recyclerView.swapAdapter(rcAdapter, false);

            cartList = new ArrayList<>();*/
        }
        else {

            selectedProductTypePosition = 0;
            Master.isProductClicked = false;
            productFragmentView = inflater.inflate(R.layout.demo_fragment_product, container, false);

            dbHelper = new DBHelper(getActivity());

            //getActivity().item.setVisible(true);

            //  getActivity().setTitle(R.string.title_fragment_product);
            relativeLayout = (RelativeLayout) productFragmentView.findViewById(R.id.relativeLayout);

            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            gone_swipe= (Button) productFragmentView.findViewById(R.id.button);
            gone_swipe2= (Button) productFragmentView.findViewById(R.id.button2);
            ivImage1 = (ImageView) productFragmentView.findViewById(R.id.ivProduct1);
            ivImage2 = (ImageView) productFragmentView.findViewById(R.id.ivProduct2);
            ivImage3 = (ImageView) productFragmentView.findViewById(R.id.ivProduct3);
            ivImage4 = (ImageView) productFragmentView.findViewById(R.id.ivProduct4);
            //ivImage1.setImageURI(Uri.parse("http://edu.par.com.pk/wp-content/uploads/2015/04/Baklazanai_Eggplanat.jpg"));
            Glide.with(getContext())
                    .load("http://ruralict.cse.iitb.ac.in/Downloads/images/silly_veggie_2.jpg")
                    .placeholder(R.drawable.placeholder_products)
                    .centerCrop()
                    .into(ivImage1);
            Glide.with(getContext())
                    .load("http://ruralict.cse.iitb.ac.in/Downloads/images/silly_veggie_1.jpg")
                    .placeholder(R.drawable.placeholder_products)
                    .centerCrop()
                    .into(ivImage2);
            Glide.with(getContext())
                    .load("http://ruralict.cse.iitb.ac.in/Downloads/images/silly_veggie_3.jpg")
                    .placeholder(R.drawable.placeholder_products)
                    .centerCrop()
                    .into(ivImage3);
            Glide.with(getContext())
                    .load("http://ruralict.cse.iitb.ac.in/Downloads/images/silly_veggie_4.jpg")
                    .placeholder(R.drawable.placeholder_products)
                    .centerCrop()
                    .into(ivImage4);
            ImageView ivbuy = (ImageView) productFragmentView.findViewById(R.id.ivBuy1);

            btnClickMe1 = (ImageView) productFragmentView.findViewById(R.id.ivBuy1);
            btnClickMe2 = (TextView) productFragmentView.findViewById(R.id.tQuantity1);
            btnClickMe3 = (TextView) productFragmentView.findViewById(R.id.tPrice1);
            swipe = (ImageView) productFragmentView.findViewById(R.id.imageView1);
            swipe2 = (ImageView) productFragmentView.findViewById(R.id.imageView2);




            result1 = new DrawerBuilder()
                    .withActivity(getActivity())
                    .withDisplayBelowStatusBar(true)
                    .withTranslucentStatusBar(false)


                    .withHasStableIds(true).addDrawerItems(
                            new PrimaryDrawerItem().withName(R.string.menu_products).withIcon(R.drawable.products).withIdentifier(0),
                            new ExpandableDrawerItem().withName(R.string.menu_orders).withIcon(R.drawable.orders).withSelectable(false).withSubItems(
                                    new SecondaryDrawerItem().withName(R.string.menu_placed_orders).withLevel(5).withIdentifier(1),
                                    new SecondaryDrawerItem().withName(R.string.menu_processed_orders).withLevel(5).withIdentifier(2)
                            ),
                            new PrimaryDrawerItem().withName(R.string.menu_profile).withIcon(R.drawable.profile).withIdentifier(11),
                            new PrimaryDrawerItem().withName(R.string.menu_change_organisation).withIcon(R.drawable.organisation).withIdentifier(3),
                            new PrimaryDrawerItem().withName(R.string.menu_referrals).withIcon(R.drawable.referrals).withIdentifier(4),
                            new PrimaryDrawerItem().withName(R.string.menu_about_us).withIcon(R.drawable.about_us).withIdentifier(5),
                            new ExpandableDrawerItem().withName(R.string.menu_help).withIcon(R.drawable.help).withSelectable(false).withSubItems(
                                    new SecondaryDrawerItem().withName(R.string.menu_feedback).withLevel(5).withIdentifier(6),
                                    new SecondaryDrawerItem().withName(R.string.menu_contact_us).withLevel(5).withIdentifier(7),
                                    new SecondaryDrawerItem().withName(R.string.menu_terms_and_conditions).withLevel(5).withIdentifier(8)))

                    .buildForFragment();




            // SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

            // if (settings.getBoolean("my_first_time", true)) {
            mTourGuideHandler = TourGuide.init(getActivity()).with(TourGuide.Technique.Click)
                    .setPointer(new Pointer().setColor(Color.RED))
                    .setToolTip(
                            new ToolTip()
                                    .setTitle("Click to buy")
                                    .setDescription("Add to Cart!")
                                    .setBackgroundColor(Color.parseColor("#4b4b4b"))
                                    .setShadow(true)
                    )
                    .setOverlay(new Overlay())
                    .playOn(btnClickMe1);

            //    settings.edit().putBoolean("my_first_time", false).commit();
            // }






            // DashboardActivity.updateSearchAdapter();

            //setting up the navigation drawer
            System.out.println("setting up the navigation drawer");
/*
        result1 = new DrawerBuilder().addDrawerItems(new PrimaryDrawerItem().withName(R.string.menu_products).withIcon(R.drawable.products).withIdentifier(0),
                new PrimaryDrawerItem().withName(R.string.menu_profile).withIcon(R.drawable.profile).withIdentifier(11)).
                withActivity(getActivity())
                .withRootView(relativeLayout)
                .withDisplayBelowStatusBar(false)
                .withSavedInstance(savedInstanceState)
                .withDrawerGravity(Gravity.END)
                .buildForFragment();


        result1.getDrawerLayout().setFitsSystemWindows(true);
        result1.getSlider().setFitsSystemWindows(true);

*/

            btnClickMe1.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    clickMe1(v);

                }});
            btnClickMe2.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    clickMe2(v);

                }});
            btnClickMe3.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    clickMe3(v);

                }});
            gone_swipe.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    // clickMe4(v);

                    mTourGuideHandler.cleanUp();
                    swipe.setVisibility(View.GONE);
                    result = new DrawerBuilder()
                            .withActivity(getActivity())



                            .addDrawerItems(
                                    new PrimaryDrawerItem().withName(R.string.menu_products).withIcon(R.drawable.products).withIdentifier(0),
                                    new ExpandableDrawerItem().withName(R.string.menu_orders).withIcon(R.drawable.orders).withSelectable(false).withSubItems(
                                            new SecondaryDrawerItem().withName(R.string.menu_placed_orders).withLevel(5).withIdentifier(1),
                                            new SecondaryDrawerItem().withName(R.string.menu_processed_orders).withLevel(5).withIdentifier(2)
                                    ),

                                    new PrimaryDrawerItem().withName(R.string.menu_logout).withIdentifier(10)
                            )




                            .withActivity(getActivity())
                            .withDrawerGravity(Gravity.END)
                            //.withSavedInstance(savedInstanceState)

                            .withRootView(relativeLayout)
                            .buildForFragment();

                    result.getDrawerLayout().setFitsSystemWindows(true);
                    result.getSlider().setFitsSystemWindows(true);


                    result.openDrawer();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            result.closeDrawer();



                        }
                    }, 1000);




                    new Pointer().setColor(Color.RED).setGravity(Gravity.LEFT);


                    mTourGuideHandler.setPointer(new Pointer().setColor(Color.RED))
                            .setToolTip(new ToolTip()
                                    .setTitle("Swipe left to right to access app's features")
                                    .setDescription(" And Close Tutorial")
                                    .setBackgroundColor(Color.parseColor("#4b4b4b"))
                            )
                            .playOn(gone_swipe2);


                    swipe2.setVisibility(View.VISIBLE);




                }});





            gone_swipe2.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    // clickMe4(v);

                    mTourGuideHandler.cleanUp();


                    result1.openDrawer();
                    // mTourGuideHandler.cleanUp();

                    // Thread.sleep(5000);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            SharedPreferenceConnector.writeBoolean(getActivity(), Master.INTRO, false);


                            if(getActivity().getIntent().hasExtra("redirectto")){
                                String redirectto = getActivity().getIntent().getStringExtra("redirectto");
                                Intent i;
                                switch(redirectto) {

                                    case "profile": {
                                        //go to profile activity
                                        i = new Intent(getActivity(), ProfileActivity.class);
                                        startActivity(i);
                                        getActivity().finish();
                                        break;
                                    }
                                    case "organisation": {
                                        //change organisation is a fragment and can't be loaded here so,
                                        //go to dashboard activity and tell it to load change organisation fragment
                                        i = new Intent(getActivity(), DashboardActivity.class);
                                        i.putExtra("redirectto", "organisation");
                                        startActivity(i);
                                        getActivity().finish();
                                        break;
                                    }
                                    case "dashboard": {
                                        //i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                                        i = new Intent(getActivity(), DashboardActivity.class);
                                        i.putExtra("redirectto" , redirectto);
                                        startActivity(i);
                                        getActivity().finish();
                                        break;
                                    }
                                }


                            }else {
                                startActivity(new Intent(getActivity(), DashboardActivity.class));
                                getActivity().finish();
                            }

                        }
                    }, 1000);

                    swipe2.setVisibility(View.GONE);



                }});







            TextView price = (TextView) productFragmentView.findViewById(R.id.tQuantity1);


            //recyclerView.setEnabled(false);
            ivNoProduct = (ImageView) productFragmentView.findViewById(R.id.ivNoProduct);
            tNoProduct = (TextView) productFragmentView.findViewById(R.id.tNoProduct);

            ivNoProduct.setVisibility(View.GONE);
            tNoProduct.setVisibility(View.GONE);

            ivNoData = (ImageView) productFragmentView.findViewById(R.id.ivNoProduct);
            tNoData = (TextView) productFragmentView.findViewById(R.id.tNoProduct);

            ivNoData.setVisibility(View.GONE);
            tNoData.setVisibility(View.GONE);

            drawerBuilder = new DrawerBuilder();

            Master.cartList = new ArrayList<>();

            setHasOptionsMenu(true);


            // new GetProductsTask(savedInstanceState).execute();
        }

        return productFragmentView;
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        //getActivity().getMenuInflater().inflate(R.menu.cart_menu_white, menu);
/*
        MenuItem item = menu.findItem(R.id.action_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        Master.setBadgeCount(getActivity(), icon, Master.CART_ITEM_COUNT);*/


    }

    @Override
    public void onResume() {
        super.onResume();

        /*Master.productList = productTypes.get(selectedProductTypePosition).productItems;
        Master.cartList = dbHelper.getCartDetails(MemberDetails.getMobileNumber(), MemberDetails.getSelectedOrgAbbr());
        updateProductList();*/

        Master.isProductClicked = false;
        Log.e("Product frag", "in OnResume");


    }

    public void clickMe1(View view) {
        mTourGuideHandler.cleanUp();

        mTourGuideHandler
                .setToolTip(new ToolTip()
                        .setTitle("Quantity")
                        .setDescription("Count of particular item in cart")
                        .setBackgroundColor(Color.parseColor("#4b4b4b")))
                .playOn(btnClickMe2);
    }

    public void clickMe2(View view) {
        mTourGuideHandler.cleanUp();

        mTourGuideHandler.setPointer(new Pointer().setColor(Color.RED))
                .setToolTip(new ToolTip()
                        .setTitle("Price")
                        .setDescription("Unit price")
                        .setBackgroundColor(Color.parseColor("#4b4b4b"))
                )
                .playOn(btnClickMe3);
    }

    public void clickMe3(View view) {
        mTourGuideHandler.cleanUp();

        new Pointer().setColor(Color.RED).setGravity(Gravity.RIGHT);
        mTourGuideHandler
                .setToolTip(new ToolTip()
                        .setTitle("Swipe right to left to change the product type")

                        .setDescription(" ")
                        .setGravity(Gravity.LEFT)
                        .setBackgroundColor(Color.parseColor("#4b4b4b"))
                )
                .playOn(gone_swipe);
        swipe.setVisibility(View.VISIBLE);



    }
    public void clickMe4(View view) {
        mTourGuideHandler.cleanUp();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {





            }
        }, 2000);




    }




    public class GetProductsTask extends AsyncTask<Void, String, String> {

        Bundle savedInstanceState;

        public GetProductsTask(Bundle savedInstanceState) {
            this.savedInstanceState = savedInstanceState;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_fetching_products), false);
        }

        @Override
        protected String doInBackground(Void... params) {

            Master.getJSON = new GetJSON();
            String url = Master.getProductsURL(dbHelper.getSelectedOrg(MemberDetails.getMobileNumber())[0]);
            System.out.println("URL: " + url);
            Master.response = Master.getJSON.getJSONFromUrl(url, null, "GET", true,MemberDetails.getEmail(), MemberDetails.getPassword());
            System.out.println(Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response)
        {
            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(DemoProductFragment.this.isAdded())
            {
                if(!response.equals("exception"))
                {
                    try {
                        JSONObject object = new JSONObject(response);
                        if(object.length() > 0)
                        {
                            Master.productTypeList = new ArrayList<>();
                            Iterator<String> keysItr = object.keys();
                            while(keysItr.hasNext())
                            {
                                String key = keysItr.next();
                                productType = new ProductType(key);
                                Object category = object.get(key);
                                System.out.println("Product type: " + productType.getName());
                                if(category instanceof JSONArray)
                                {
                                    for(int i=0; i<((JSONArray)category).length();i++)
                                    {
                                        if(((JSONArray)category).getJSONObject(i).has("imageUrl") && ((JSONArray)category).getJSONObject(i).has("audioUrl"))
                                        {
                                            if(((JSONArray)category).getJSONObject(i).getString("imageUrl") == null && ((JSONArray)category).getJSONObject(i).getString("audioUrl") == null)
                                            {
                                                product =new Product(((JSONArray)category).getJSONObject(i).getString("name"),
                                                        Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),
                                                        0.0,
                                                        Integer.parseInt(((JSONArray)category).getJSONObject(i).getString("quantity")),
                                                        ((JSONArray)category).getJSONObject(i).getString("stockManagement"), "null", "null",
                                                        ((JSONArray)category).getJSONObject(i).getString("id"));
                                            }
                                            else if(((JSONArray)category).getJSONObject(i).getString("imageUrl") == null)
                                            {
                                                product=new Product(((JSONArray)category).getJSONObject(i).getString("name"),
                                                        Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),
                                                        0.0,
                                                        Integer.parseInt(((JSONArray)category).getJSONObject(i).getString("quantity")),
                                                        ((JSONArray)category).getJSONObject(i).getString("stockManagement"),
                                                        "null",
                                                        ((JSONArray)category).getJSONObject(i).getString("audioUrl"),
                                                        ((JSONArray)category).getJSONObject(i).getString("id"));
                                            }
                                            else if (((JSONArray)category).getJSONObject(i).getString("audioUrl") == null)
                                            {
                                                product=new Product(((JSONArray)category).getJSONObject(i).getString("name"),
                                                        Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),
                                                        0.0,
                                                        Integer.parseInt(((JSONArray)category).getJSONObject(i).getString("quantity")),
                                                        ((JSONArray)category).getJSONObject(i).getString("stockManagement"),
                                                        ((JSONArray)category).getJSONObject(i).getString("imageUrl"),
                                                        "null",
                                                        ((JSONArray)category).getJSONObject(i).getString("id"));
                                            }
                                            else
                                            {
                                                product=new Product(((JSONArray)category).getJSONObject(i).getString("name"),
                                                        Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),
                                                        0.0,
                                                        Integer.parseInt(((JSONArray)category).getJSONObject(i).getString("quantity")),
                                                        ((JSONArray)category).getJSONObject(i).getString("stockManagement"),
                                                        ((JSONArray)category).getJSONObject(i).getString("imageUrl"),
                                                        ((JSONArray)category).getJSONObject(i).getString("audioUrl"),
                                                        ((JSONArray)category).getJSONObject(i).getString("id"));
                                            }
                                        }
                                        else if(((JSONArray)category).getJSONObject(i).has("imageUrl"))
                                        {
                                            if(((JSONArray)category).getJSONObject(i).getString("imageUrl") == null)
                                            {
                                                product=new Product(((JSONArray)category).getJSONObject(i).getString("name"),
                                                        Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),
                                                        0.0,
                                                        Integer.parseInt(((JSONArray)category).getJSONObject(i).getString("quantity")),
                                                        ((JSONArray)category).getJSONObject(i).getString("stockManagement"),
                                                        "null",
                                                        "null",
                                                        ((JSONArray)category).getJSONObject(i).getString("id"));
                                            }
                                            else
                                            {
                                                product=new Product(((JSONArray)category).getJSONObject(i).getString("name"),
                                                        Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),
                                                        0.0,
                                                        Integer.parseInt(((JSONArray)category).getJSONObject(i).getString("quantity")),
                                                        ((JSONArray)category).getJSONObject(i).getString("stockManagement"),
                                                        ((JSONArray)category).getJSONObject(i).getString("imageUrl"),
                                                        "null",
                                                        ((JSONArray)category).getJSONObject(i).getString("id"));
                                            }
                                        }
                                        else if(((JSONArray)category).getJSONObject(i).has("audioUrl"))
                                        {
                                            if(((JSONArray)category).getJSONObject(i).getString("audioUrl") == null)
                                            {
                                                product=new Product(((JSONArray)category).getJSONObject(i).getString("name"),
                                                        Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),
                                                        0.0,
                                                        Integer.parseInt(((JSONArray)category).getJSONObject(i).getString("quantity")),
                                                        ((JSONArray)category).getJSONObject(i).getString("stockManagement"),
                                                        "null",
                                                        "null",
                                                        ((JSONArray)category).getJSONObject(i).getString("id"));
                                            }
                                            else
                                            {
                                                product=new Product(((JSONArray)category).getJSONObject(i).getString("name"),
                                                        Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),
                                                        0.0,
                                                        Integer.parseInt(((JSONArray)category).getJSONObject(i).getString("quantity")),
                                                        ((JSONArray)category).getJSONObject(i).getString("stockManagement"),
                                                        "null",
                                                        ((JSONArray)category).getJSONObject(i).getString("audioUrl"),
                                                        ((JSONArray)category).getJSONObject(i).getString("id"));
                                            }
                                        }
                                        else
                                        {
                                            product=new Product(((JSONArray)category).getJSONObject(i).getString("name"),
                                                    Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),
                                                    0.0,
                                                    Integer.parseInt(((JSONArray)category).getJSONObject(i).getString("quantity")),
                                                    ((JSONArray)category).getJSONObject(i).getString("stockManagement"),
                                                    "null",
                                                    "null",
                                                    ((JSONArray)category).getJSONObject(i).getString("id"));
                                        }

                                        stockEnabledStatus = ((JSONArray)category).getJSONObject(i).getString("stockManagement");
                                        productType.productItems.add(product);
                                    }
                                }

                                int i=0; // to set the selection dynamically afterwards
                                if(productType.productItems.size() > 0)
                                {
                                    drawerBuilder.addDrawerItems(new PrimaryDrawerItem().withName(productType.getName()).withIdentifier(i++));
                                    Log.e("Product frg", productType.getName() + " added to drawer");
                                    Master.productTypeList.add(productType);
                                }
                            }

                            DashboardActivity.updateSearchAdapter();

                            //setting up the navigation drawer
                            System.out.println("setting up the navigation drawer");

                            result = drawerBuilder
                                    .withActivity(getActivity())
                                    .withRootView(relativeLayout)
                                    .withDisplayBelowStatusBar(false)
                                    .withSavedInstance(savedInstanceState)
                                    .withDrawerGravity(Gravity.END)
                                    .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {

                                        @Override

                                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                                            drawerItemClick(position);
                                            return true;
                                        }

                                    })
                                    .buildForFragment();


                            result.getDrawerLayout().setFitsSystemWindows(true);
                            result.getSlider().setFitsSystemWindows(true);


                            Master.productList = Master.productTypeList.get(0).productItems;
                            Master.cartList = dbHelper.getCartDetails(MemberDetails.getMobileNumber(), MemberDetails.getSelectedOrgAbbr());
                            updateCart();
                            updateProductList();

                            ProductAdapter rcAdapter = new ProductAdapter(getActivity());
                            recyclerView.setAdapter(rcAdapter);
                            getActivity().setTitle(Master.productTypeList.get(0).getName());





                        }
                        else
                        {
                            //Material.alertDialog(getActivity(), getString(R.string.alert_no_products), "OK");
                            recyclerView.setVisibility(View.GONE);
                            ivNoProduct.setVisibility(View.VISIBLE);
                            tNoProduct.setVisibility(View.VISIBLE);
                        }
                    }
                    catch (JSONException e)
                    {
                        Log.e("Product frag", "Inside catch. " + e.getMessage());
                    }
                }
                else
                {
                    // if exception
                    recyclerView.setVisibility(View.GONE);
                    ivNoData.setVisibility(View.VISIBLE);
                    tNoData.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void setSelection(int position)
    {
        Log.e("Product frag", "in setSelection position: " + position);
        selectedProductTypePosition = position;
    }

    public void drawerItemClick(int position)
    {
        result.closeDrawer();
        Log.e("Product frag", "Drawer clicked: " + position);

        selectedProductTypePosition = position;
        Master.productList = Master.productTypeList.get(position).productItems;

        updateProductList();

        swapAdapter = new ProductAdapter(getActivity());
        recyclerView.swapAdapter(swapAdapter, false);
        getActivity().setTitle(Master.productTypeList.get(position).getName());
    }

    public void updateProductList()
    {
        for(int i = 0; i < Master.cartList.size(); ++i)
        {
            for(int j = 0; j< Master.productList.size(); ++j)
            {
                if(Master.productList.get(j).getID().equals(Master.cartList.get(i).getID()))
                {
                    Master.productList.get(j).setQuantity(Master.cartList.get(i).getQuantity());
                    break;
                }
            }
        }
    }


    public void updateCart()
    {
        for(int i = 0; i < Master.productTypeList.size(); ++i)
        {
            for (int j = 0; j < Master.productTypeList.get(i).productItems.size(); ++j)
            {

                for(int z = 0; z< Master.cartList.size(); z++)
                {
                    if(Master.cartList.get(z).getID()== Master.productTypeList.get(i).productItems.get(j).getID())
                    {
                        Master.cartList.get(z).setName(Master.productTypeList.get(i).productItems.get(j).getName());
                        Master.cartList.get(z).setUnitPrice(Master.productTypeList.get(i).productItems.get(j).getUnitPrice());
                        Master.cartList.get(z).setImageUrl(Master.productTypeList.get(i).productItems.get(j).getImageUrl());
                        Master.cartList.get(z).setStockQuantity(Master.productTypeList.get(i).productItems.get(j).getStockQuantity());
                        Master.cartList.get(z).setStockEnabledStatus(Master.productTypeList.get(i).productItems.get(j).getStockEnabledStatus());

                        String total = String.format("%.2f", Master.productTypeList.get(i).productItems.get(j).getUnitPrice()* Master.cartList.get(z).getQuantity());

                        Master.cartList.get(z).setTotal(Double.parseDouble(total));

                        dbHelper.updateProduct(
                                String.valueOf( Master.cartList.get(z).getUnitPrice()),
                                String.valueOf( Master.cartList.get(z).getQuantity()),
                                String.valueOf( Master.cartList.get(z).getTotal()),
                                String.valueOf( Master.cartList.get(z).getName()),
                                MemberDetails.getMobileNumber(),
                                MemberDetails.getSelectedOrgAbbr(),
                                String.valueOf( Master.cartList.get(z).getID()),
                                String.valueOf( Master.cartList.get(z).getImageUrl()),
                                String.valueOf(Master.cartList.get(z).getStockQuantity()),
                                Master.cartList.get(z).getStockEnabledStatus()
                        );

                    }
                }

            }
        }
    }


}