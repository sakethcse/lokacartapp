package com.mobile.ict.cart.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.fragment.AboutUsFragment;
import com.mobile.ict.cart.fragment.OrganisationFragment;
import com.mobile.ict.cart.fragment.ContactUsFragment;
import com.mobile.ict.cart.fragment.FAQFragment;
import com.mobile.ict.cart.fragment.FeedbackFragment;
import com.mobile.ict.cart.fragment.PlacedOrderFragment;
import com.mobile.ict.cart.fragment.ProcessedOrderFragment;
import com.mobile.ict.cart.fragment.ProductFragment;
import com.mobile.ict.cart.fragment.ProfileFragment;
import com.mobile.ict.cart.fragment.ReferralFragment;
import com.mobile.ict.cart.fragment.TermsAndConditionsFragment;
import com.mobile.ict.cart.gcm.MyGcmListenerService;
import com.mobile.ict.cart.gcm.RegistrationIntentService;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.container.MemberDetails;

import java.util.ArrayList;

public class DashboardActivity extends AppCompatActivity{

    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private long mBackPressed = 0;

    FragmentManager fragmentManager;
    NavigationView navigationView;
    static TextView drawerName;
    static TextView drawerEmail;
    static TextView drawerMobileNumber;
    ImageView ivEdit;
    View headerView;
    public static Boolean backpress = false;
    RelativeLayout relativeLayout;
    Drawer result;
    AccountHeaderBuilder accountHeaderBuilder;
    Toolbar toolbar;
    DBHelper dbHelper;
    static LayerDrawable icon;
    MenuItem item;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    //adapter to set the list view in search functionality
    public static ArrayAdapter<String> itemsAdapter;

    //list to store products based on entered query
    public static ArrayList<String> newProducts;

    //list of products and their positions
    public static ArrayList<String> products;
    public static ArrayList<String> products_id;

    //declare a variable to get query in search field
    String query_entered=null;




    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        dbHelper = new DBHelper(this);

        backpress = false;
        Master.initialise(getApplicationContext());

        Master.productList = new ArrayList<>();
        Master.getJSON = new GetJSON();

        setContentView(R.layout.activity_dashboard);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        relativeLayout = (RelativeLayout) findViewById(R.id.dashboardRelativeLayout);

        fragmentManager = getSupportFragmentManager();

        headerView = getLayoutInflater().inflate(R.layout.nav_header_dashboard, null);

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withHeader(headerView)
                /*.inflateMenu(R.menu.activity_dashboard_drawer)*/
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.menu_products).withIcon(R.drawable.products).withIdentifier(0),
                        new ExpandableDrawerItem().withName(R.string.menu_orders).withIcon(R.drawable.orders).withSelectable(false).withSubItems(
                                new SecondaryDrawerItem().withName(R.string.menu_placed_orders).withLevel(5).withIdentifier(1),
                                new SecondaryDrawerItem().withName(R.string.menu_processed_orders).withLevel(5).withIdentifier(2)
                        ),
                        new PrimaryDrawerItem().withName(R.string.menu_profile).withIcon(R.drawable.profile).withIdentifier(11),
                        new PrimaryDrawerItem().withName(R.string.menu_change_organisation).withIcon(R.drawable.organisation).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.menu_referrals).withIcon(R.drawable.referrals).withIdentifier(4),
                        new PrimaryDrawerItem().withName(R.string.menu_about_us).withIcon(R.drawable.about_us).withIdentifier(5),
                        new ExpandableDrawerItem().withName(R.string.menu_help).withIcon(R.drawable.help).withSelectable(false).withSubItems(
                                new SecondaryDrawerItem().withName(R.string.menu_feedback).withLevel(5).withIdentifier(6),
                                new SecondaryDrawerItem().withName(R.string.menu_contact_us).withLevel(5).withIdentifier(7),
                                new SecondaryDrawerItem().withName(R.string.menu_terms_and_conditions).withLevel(5).withIdentifier(8)
                                /*new SecondaryDrawerItem().withName(R.string.menu_faqs).withLevel(2).withIdentifier(9)*/
                        )
                        /*new PrimaryDrawerItem().withName(R.string.menu_logout).withIdentifier(10)*/
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        Log.e("DashboardAct", "Position: " + drawerItem.getIdentifier());
                        dashBoardClickListener(drawerItem.getIdentifier());

                        backpress = false;

                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();

        drawerName = (TextView) headerView.findViewById(R.id.tDashboardName);
        drawerName.setText(MemberDetails.getFname() + " " + MemberDetails.getLname());

        drawerEmail = (TextView) headerView.findViewById(R.id.tDashboardEmail);
        drawerEmail.setText(MemberDetails.getEmail());

        drawerMobileNumber = (TextView) headerView.findViewById(R.id.tDashboardMobileNumber);
        drawerMobileNumber.setText(MemberDetails.getMobileNumber());

        ivEdit = (ImageView) headerView.findViewById(R.id.ivEdit);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.beginTransaction().replace(R.id.content_frame, new ProfileFragment(), Master.PROFILE_TAG).commit();
                result.setSelection(11);
                result.closeDrawer();
            }
        });

        try
        {
            if(getIntent().getStringExtra("redirectto").equals("processed"))
            {
                fragmentManager.beginTransaction().replace(R.id.content_frame, new ProcessedOrderFragment(), Master.PROCESSED_ORDER_TAG).commit();
                //result.setSelection(2);
                Log.e("-- intent data dash", "processed");
            }
            else if(getIntent().getStringExtra("redirectto").equals("organisation"))
            {
                //fragmentManager.beginTransaction().replace(R.id.content_frame, new OrganisationFragment(), Master.CHANGE_ORGANISATION_TAG).commit();
                result.setSelection(3);
                Log.e("-- intent data dash", "organisation");
            }
            else
            {
                fragmentManager.beginTransaction().replace(R.id.content_frame, new ProductFragment(), Master.PRODUCT_TAG).commit();
                Log.e("-- intent data dash", "in if product");
            }
        }
        catch (Exception e)
        {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ProductFragment(), Master.PRODUCT_TAG).commit();
            Log.e("-- intent data dash", "in catch product");
        }

        result.closeDrawer();


        Intent intent = new Intent(this, RegistrationIntentService.class);
        if (checkPlayServices())
        {
            startService(intent);
        }
    }



    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                finish();
            }
            return false;
        }
        return true;
        //fragmentManager.beginTransaction().replace(R.id.content_frame, new ProductFragment(), Master.PRODUCT_TAG).commit();
    }

    void dashBoardClickListener(long position)
    {

        if (position == 0)
        {
            result.getAdapter().collapse();
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ProductFragment(), Master.PRODUCT_TAG).commit();
        }
        else if (position == 1)
        {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new PlacedOrderFragment(), Master.PLACED_ORDER_TAG).commit();
        }
        else if (position == 2)
        {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ProcessedOrderFragment(), Master.PROCESSED_ORDER_TAG).commit();
        }
        else if (position == 3)
        {
            result.getAdapter().collapse();
            fragmentManager.beginTransaction().replace(R.id.content_frame, new OrganisationFragment(), Master.CHANGE_ORGANISATION_TAG).commit();
        }
        else if (position == 4)
        {
            result.getAdapter().collapse();
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ReferralFragment(), Master.REFERRALS_TAG).commit();
        }
        else if(position == 5)
        {
            result.getAdapter().collapse();
            fragmentManager.beginTransaction().replace(R.id.content_frame, new AboutUsFragment(), Master.ABOUT_US_TAG).commit();
        }
        else if (position == 6)
        {
            result.getAdapter().collapse();
            fragmentManager.beginTransaction().replace(R.id.content_frame, new FeedbackFragment(), Master.FEEDBACKS_TAG).commit();
        }
        else if (position == 7)
        {
            // DialogFragment.show() will take care of adding the fragment
            // in a transaction.  We also want to remove any currently showing
            // dialog, so make our own transaction and take care of that here.
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            FragmentManager fm = this.getSupportFragmentManager();
            // Create and show the dialog.
            ContactUsFragment newFragment = new ContactUsFragment();
            newFragment.show(fm, "dialog");
        }
        else if (position == 8)
        {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new TermsAndConditionsFragment(), Master.TERMS_AND_CONDITIONS_TAG).commit();
        }
        else if (position == 9)
        {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new FAQFragment(), Master.FAQ_TAG).commit();
        }
        else if(position == 10)
        {
            dbHelper.clearLogin();
            startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
            finish();
        }
        else if(position == 11)
        {
            ProfileFragment profileFragment = (ProfileFragment) getSupportFragmentManager().findFragmentByTag(Master.PROFILE_TAG);
            if(profileFragment == null)
                fragmentManager.beginTransaction().replace(R.id.content_frame, new ProfileFragment(), Master.PROFILE_TAG).commit();
        }

    }

    public static void updateSearchAdapter()
    {
        products_id = new ArrayList<>();
        for(int i = 0; i < Master.productTypeList.size(); ++i)
        {
            for(int j = 0; j < Master.productTypeList.get(i).productItems.size(); ++j)
            {
                products.add(Master.productTypeList.get(i).productItems.get(j).getName());
                products_id.add(Master.productTypeList.get(i).productItems.get(j).getID());
            }
        }
        itemsAdapter.notifyDataSetChanged();
    }

    int getPositionFromName(String name)
    {
        for(int i = 0; i < Master.productTypeList.size(); ++i)
        {
            for(int j = 0; j < Master.productTypeList.get(i).productItems.size(); ++j)
            {
                if(Master.productTypeList.get(i).productItems.get(j).getName().equals(name))
                {
                    Master.productList = Master.productTypeList.get(i).productItems;
                    setTitle(Master.productTypeList.get(i).getName());
                    ProductFragment productFragment = (ProductFragment) getSupportFragmentManager().findFragmentByTag(Master.PRODUCT_TAG);
                    if(productFragment == null)
                        Log.e("Dash act", "null prod frag");
                    else
                        productFragment.setSelection(i);
                    return j;
                }
            }
        }
        return -1;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        //code for search fucntionality


        products = new ArrayList<>();
        itemsAdapter = new ArrayAdapter<String>(this,R.layout.search_list_item,R.id.product_name, products);

        final ListView listView = (ListView) findViewById(R.id.search_list_view);

        if(itemsAdapter==null)Log.e("Dash act","itemsadapter null" );
        else Log.e("Dash act","itemsadapter not null");

        listView.setAdapter(itemsAdapter);
        getMenuInflater().inflate(R.menu.cart_menu_white, menu);



        item = menu.findItem(R.id.action_cart);
        icon = (LayerDrawable) item.getIcon();
        Master.setBadgeCount(this, icon, Master.CART_ITEM_COUNT);

        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchMenuItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        //When clicked on back button after search, replaces frame with product details instead of search suggestions
        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Write your code here

                FrameLayout layout = (FrameLayout)findViewById(R.id.content_frame);
                layout.setVisibility(View.VISIBLE);

                FrameLayout layout2 = (FrameLayout)findViewById(R.id.search_framelayout);
                layout2.setVisibility(View.GONE);

                return true;
            }
        });

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);



        //when you change text in query field
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }



            @Override
            public boolean onQueryTextChange(String newText) {

                query_entered=newText;

                if (TextUtils.isEmpty(newText)) {
                    listView.clearTextFilter();
                    itemsAdapter.getFilter().filter("");
                    //if query is cleared list view should show all suggestions which is in itemsadapter
                    listView.setAdapter(itemsAdapter);
                } else {

                    //code to display filtered results based on query entered

                    //clear the list view before everytime new query entered
                    listView.setAdapter(null);

                    //now update the listView based on entered query
                    //always create a new adapter(initialized to null) when new query is entered and update it and add it to listView
                    // now populate the list view with only set of items that start with characters in query entered
                    //to obtain this loop over all items and get the required items to add to listView
                    newProducts=new ArrayList<>();
                    ArrayAdapter<String> newitemsAdapter = new ArrayAdapter<String>(DashboardActivity.this,R.layout.search_list_item,R.id.product_name, newProducts);


                    for(int i = 0; i < products.size(); i++) {
                        String item = products.get(i).toLowerCase();
                        String qr = newText.toLowerCase();

                        if(item.startsWith(qr)) {

                            newitemsAdapter.add( item );

                        }
                    }

                    //now update list view with new adapter
                    listView.setAdapter(newitemsAdapter);

                }

                return true;
            }
        });

        //to collapse query field when not in use
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean queryTextFocused) {
                if(!queryTextFocused) {
                    searchMenuItem.collapseActionView();
                    searchView.setQuery("", false);
                }
            }
        });



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                //when clicked on search results,it should redirect to that particular product page

                //write a code here to map position to correct position in product list

                //get the value of query and store the original postions of search results

                ArrayList<Integer> newPosition=new ArrayList<>();;
                for(int i = 0; i < products.size(); i++) {
                    String item = products.get(i).toLowerCase();
                    String qr = query_entered.toLowerCase();


                    if(item.startsWith(qr)) {

                        newPosition.add(i);

                    }
                }


                //now update the position to correct value stored previously
                position=newPosition.get(position);


                // TODO Auto-generated method stub
                Toast.makeText(DashboardActivity.this, products.get(position), Toast.LENGTH_SHORT).show();

                FrameLayout layout = (FrameLayout) findViewById(R.id.content_frame);
                layout.setVisibility(View.VISIBLE);

                FrameLayout layout2 = (FrameLayout) findViewById(R.id.search_framelayout);
                layout2.setVisibility(View.GONE);


                //to close search menu when clicked on back from product details page
                searchMenuItem.collapseActionView();
                searchView.setQuery("", false);


                Log.e("Dash Act", "Position: " + position + ".  Name: " + products.get(position));

                Intent i = new Intent(DashboardActivity.this, ProductDetailActivity.class);

                i.putExtra("position", getPositionFromName(products.get(position)));
                startActivity(i);
            }
        });



        return true;

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.e("Dash act", "menu click: " + item.toString() + " " + item.getItemId());

        switch (item.getItemId()) {

            case R.id.action_cart:
                startActivity(new Intent(DashboardActivity.this, CartActivity.class));
                return true;

            case R.id.search:
                Log.e("Dash act", "search clicked");
                FrameLayout layout = (FrameLayout)findViewById(R.id.content_frame);
                layout.setVisibility(View.GONE);

                FrameLayout layout2 = (FrameLayout)findViewById(R.id.search_framelayout);
                layout2.setVisibility(View.VISIBLE);

                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

   /* public static void updateCart()
    {
        Master.setBadgeCount(, icon, Master.CART_ITEM_COUNT);
    }*/





    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(MyGcmListenerService.ACTION));

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor;
        if(sharedPrefs.contains("orderNotifs"))
        {
            editor = sharedPrefs.edit();
            editor.remove("orderNotifs");
            editor.apply();
        }
        if(sharedPrefs.contains("memberNotifs"))
        {
            editor = sharedPrefs.edit();
            editor.remove("memberNotifs");
            editor.apply();
        }

        Master.CART_ITEM_COUNT=  dbHelper.getCartItemsCount(MemberDetails.getMobileNumber(),MemberDetails.getSelectedOrgAbbr());
        System.out.println("--------ON START-------------" + Master.CART_ITEM_COUNT);
        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed() {

        if (result.isDrawerOpen()) {
            result.closeDrawer();
        }
        else if(getSupportFragmentManager().getBackStackEntryCount() == 0)
        {
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis())
            {
                finish();
                return;
            }
            else { Toast.makeText(getBaseContext(), "Tap back button in order to exit", Toast.LENGTH_SHORT).show(); }

            mBackPressed = System.currentTimeMillis();
        }
        else
            getSupportFragmentManager().popBackStack();

    }

    public static void updateNavHeader()
    {
        drawerName.setText(MemberDetails.getFname() + " " + MemberDetails.getLname());
        drawerEmail.setText(MemberDetails.getEmail());
        drawerMobileNumber.setText(MemberDetails.getMobileNumber());
    }


}
