package com.mobile.ict.cart.activity;

import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Product;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.adapter.EditPlacedOrderCartAdapter;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class EditPlacedOrderActivity extends AppCompatActivity {

    ArrayList<Product> cartList;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    LinearLayout cartButtonLinearLayout;
    RelativeLayout emptyCartLinearLayout;
    EditPlacedOrderCartAdapter editPlacedOrderCartAdapter;
    Bundle bundle;
    Map<String,String> itemsUnitRateHashMap = new HashMap<String, String>();
    HashMap<String,Integer> itemsStockQuantityHashMap = new HashMap<>();
    ArrayList<Product> itemss;
    private double Total=0.0;
    //  Dialog dialog ;
    TextView cartTotal;
    DBHelper dbHelper;
    double sum=0.0;
    Menu menu;
    String stockEnabledStatus="false";
    String response,val;
    int orderid;
    LayerDrawable icon;
    MenuItem item;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bundle = new Bundle();
        stockEnabledStatus = getIntent().getStringExtra("stockEnabledStatus");
        itemsStockQuantityHashMap = (HashMap) getIntent().getSerializableExtra("itemsStockQuantityHashMap");
        itemsUnitRateHashMap = (Map)getIntent().getSerializableExtra("itemsUnitRateHashMap");
        bundle = getIntent().getExtras();

        cartList = new ArrayList<Product>();

        Map<String, String> sortList = new TreeMap<String, String>(itemsUnitRateHashMap);
        for(Map.Entry<String, String> entry : sortList.entrySet()){
            Product item = new Product(entry.getKey(),Double.parseDouble(entry.getValue()),0.0);
            cartList.add(item);
        }

        layoutManager = new LinearLayoutManager(this);

        cartButtonLinearLayout = (LinearLayout) findViewById(R.id.cartButtonLinearLayout);

        emptyCartLinearLayout = (RelativeLayout) findViewById(R.id.cartEmptyRelativeLayout);

        recyclerView = (RecyclerView) findViewById(R.id.rvCart);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);

        emptyCartLinearLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        cartButtonLinearLayout.setVisibility(View.VISIBLE);

        cartTotal = (TextView)findViewById(R.id.tcartTotal);



            for(int i = 0; i<cartList.size(); ++i )
            {
                sum=sum+cartList.get(i).getTotal();
            }
        cartTotal.setText(""+String.format("%.2f",sum));
        editPlacedOrderCartAdapter = new EditPlacedOrderCartAdapter(this,cartList,cartTotal,bundle, itemsStockQuantityHashMap,stockEnabledStatus);
        recyclerView.setAdapter(editPlacedOrderCartAdapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }


    @Override
    public boolean onSupportNavigateUp() {

        finish();
        getSupportFragmentManager().popBackStack();
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void onCheckOut(View v)
    {

        if(checkChange())
        {
            if(Master.isNetworkAvailable(this))
            {

                boolean flag= true;
                itemss = new ArrayList<>();

                Map<String,String> itemsMap = (Map) getIntent().getSerializableExtra("itemsUnitRateHashMap");
                Map<String, String> sortList = new TreeMap<String, String>(itemsMap);

               /* if(stockEnabledStatus.equals("true"))
                {
                    for(Map.Entry<String, String> entry : sortList.entrySet())
                    {
                        int quantity = bundle.getInt(entry.getKey(), 1);
                        if(quantity > 0)
                        {

                            Map<String, Integer> stockQuantityList = new TreeMap<String, Integer>(itemsStockQuantityHashMap);
                            for(Map.Entry<String, Integer> stockEntry : stockQuantityList.entrySet())
                            {
                                if(stockEntry.getKey().equals(entry.getKey()))
                                {
                                    int stockQuantity = stockEntry.getValue();
                                    if(quantity>stockQuantity)
                                    {
                                        Toast.makeText(getApplicationContext(),R.string.label_toast_sorry_we_cannot_process_your_order_due_to_insufficent_stock,Toast.LENGTH_LONG).show();
                                        itemss.clear();
                                        Total=0.0;
                                        flag = false;
                                        break;

                                    }
                                }


                            }
                            if(flag) {
                                double total = quantity * Double.parseDouble(entry.getValue());
                                Total += Double.parseDouble( String.format("%.2f",total));
                                Product item = new Product(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), Double.parseDouble( String.format("%.2f",total)));
                                itemss.add(item);
                            }else{
                                break;
                            }




                        }
                    }


                }
*/
             //   else
                {
                    for(Map.Entry<String, String> entry : sortList.entrySet()) {
                        int quantity = bundle.getInt(entry.getKey(), 1);
                        if (quantity > 0) {
                            double total = quantity * Double.parseDouble(entry.getValue());
                            Total += Double.parseDouble(String.format("%.2f", total));
                            Product item = new Product(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), Double.parseDouble(String.format("%.2f", total)));
                            itemss.add(item);
                        }
                    }
                }




                if(itemss.isEmpty()&&flag){
                    Material.alertDialog(this, getString(R.string.alert_please_insert_quantity), "OK");
                }
                else if(!itemss.isEmpty()&&flag){

                    try {
                        JSONObject  order = new JSONObject();
                        order.put("orgabbr",MemberDetails.getSelectedOrgAbbr());
                        order.put("groupname", "Parent Group");
                        order.put("status","saved");
                        order.put("comments","null");
                        JSONArray products = new JSONArray();
                        JSONObject object;
                        int i=1;
                        for(Product item : itemss)
                        {
                            object  = new JSONObject();
                            object.put("name",item.getName());
                            object.put("quantity",item.getQuantity());
                            products.put(object);
                            i++;
                        }
                        order.put("orderItems",products);

                        System.out.println("editing placed order json------------"+order.toString());

                        orderid=getIntent().getExtras().getInt("orderId");

                        new UpdateOrderTask(EditPlacedOrderActivity.this,orderid).execute(order);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            else
            {
                Material.alertDialog(this, getString(R.string.alert_no_internet_connection_found), "OK");
            }
        }
        else
        {
            Toast.makeText(this, R.string.toast_no_changes_to_save, Toast.LENGTH_LONG).show();
        }
    }

    boolean checkChange()
    {
        Map<String,String> itemsMap = (Map) getIntent().getSerializableExtra("itemsUnitRateHashMap");
        Map<String, String> sortList = new TreeMap<String, String>(itemsMap);

        int i = 0;

        for(Map.Entry<String, String> entry : sortList.entrySet())
        {
            int quantity = bundle.getInt(entry.getKey(), 1);
            if(editPlacedOrderCartAdapter.getChangeCheck().get(i++) != quantity)
                return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_cart:
                return true;

            default:

                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();


    }


    private class UpdateOrderTask extends AsyncTask<JSONObject,String,String>
    {
        int id;

        public UpdateOrderTask(Context context,int id){
            this.id=id;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(EditPlacedOrderActivity.this, getString(R.string.pd_updating_order), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJson = new GetJSON();
          //  String serverUrl="http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/update/"+id;

            response = getJson.getJSONFromUrl(Master.getEditPlacedOrderURL(id),params[0],"POST",true,MemberDetails.getEmail(),MemberDetails.getPassword());


           // response = getJson.getJSONFromUrl(Master.getEditPlacedOrderURL(id),params[0],"POST",true,MemberDetails.getEmail(),MemberDetails.getPassword());


            System.out.println("--------detail response--------"+response);
            return response;
        }

        @Override
        protected void onPostExecute(String s) {

            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();
            if(s.equals("exception"))
            {
                Material.alertDialog(EditPlacedOrderActivity.this, getString(R.string.alert_cannot_connect_to_the_server), "OK");
            }
            else {
                JSONObject jsonObj = null;
                try {
                    jsonObj = new JSONObject(s);
                    val=jsonObj.getString("status");

                    if(val.equals("Success"))
                    {
                        Toast.makeText(EditPlacedOrderActivity.this,R.string.label_toast_Your_order_has_been_updated_successfully, Toast.LENGTH_LONG).show();
                        finish();
                        getSupportFragmentManager().popBackStack();
                    }
                    else  if(val.equals("Failure"))
                    {
                        String error[] = {jsonObj.getString("error")};

                        Toast.makeText(EditPlacedOrderActivity.this, getResources().getString(R.string.label_toast_sorry_we_are_unable_to_process_order_items)+error[0] + getResources().getString(R.string.label_toast_out_of_stock), Toast.LENGTH_LONG).show();

                    }

                    else
                        Toast.makeText(EditPlacedOrderActivity.this,R.string.label_toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }
        }
    }



}
