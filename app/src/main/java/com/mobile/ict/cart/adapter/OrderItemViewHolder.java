package com.mobile.ict.cart.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.interfaces.EditDeletePlacedOrderInterface;
import com.mobile.ict.cart.util.Master;


import java.util.ArrayList;


public class OrderItemViewHolder extends ChildViewHolder {


    ListView itemListView;
    ScrollView sv;
    Context ctx;
    ImageView edit,delete;
    EditDeletePlacedOrderInterface callback;
    RelativeLayout rlFooter;

    public OrderItemViewHolder(Context context, View itemView,EditDeletePlacedOrderInterface editDeletePlacedOrderInterface,String type)
    {
        super(itemView);
        ctx = context;
        callback= editDeletePlacedOrderInterface;
        itemListView = (ListView)itemView.findViewById(R.id.listView);
        edit=(ImageView)itemView.findViewById(R.id.edit);
        delete=(ImageView)itemView.findViewById(R.id.delete);
        rlFooter = (RelativeLayout)itemView.findViewById(R.id.footer);

        if(type.equals(Master.PROCESSEDORDER))
        {
            rlFooter.setVisibility(View.GONE);
            /*edit.setVisibility(View.INVISIBLE);
            edit.setEnabled(false);
            delete.setVisibility(View.INVISIBLE);
            delete.setEnabled(false);*/

        }
        else if(type.equals(Master.PLACEDORDER))
        {
            rlFooter.setVisibility(View.VISIBLE);
            /*edit.setVisibility(View.VISIBLE);
            edit.setEnabled(true);
            delete.setVisibility(View.VISIBLE);
            delete.setEnabled(true);*/
        }


        itemListView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

    }

    public void bind(ArrayList<String[]> list, final int orderId,final int pos,String type)
    {
        Resources r = ctx.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, r.getDisplayMetrics());
       /* RelativeLayout.LayoutParams mParam;

        if(list.size()==1)
      {
         *//* TypedValue value = new TypedValue();
          ctx.getTheme().resolveAttribute(android.R.attr.listPreferredItemHeight, value, true);
          float itemHeight = value.getDimension(ctx.getResources().getDisplayMetrics());*//*
          mParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      }
        else
        {
            mParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,(int)px);

        }*/

        LinearLayout.LayoutParams mParam;

        if(list.size()==1)
        {

            mParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        else
        {
            mParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,(int)px);

        }
        itemListView.setLayoutParams(mParam);

        itemListView.setAdapter(new DataAdapter(ctx, list));


        if(type.equals(Master.PLACEDORDER))
        {
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.deletePlacedOrder();
                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.editPlacedOrder();
                }
            });
        }

    }


    public class DataAdapter extends BaseAdapter
    {
        public ArrayList<String[]> list;
        Context context;

        public DataAdapter(Context ctx, ArrayList<String[]> list) {
            super();
            context = ctx;
            this.list = list;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        private class ViewHolder {
            TextView productName,quantity,total;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            LayoutInflater inflater =  ((Activity)context).getLayoutInflater();

            if (convertView == null)
            {
                convertView = inflater.inflate(R.layout.order_list_item, null);
                holder = new ViewHolder();
                holder.productName = (TextView) convertView.findViewById(R.id.placed_order_quantityName);
                holder.quantity = (TextView) convertView.findViewById(R.id.placed_order_quantity);
                holder.total = (TextView) convertView.findViewById(R.id.placed_order_quantityTotalPrice);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            String[] list = (String[]) getItem(position);

            System.out.println(list[0]+"---------"+list[2]+"----------"+list[3]);

            holder.productName.setText(list[0]);
            holder.quantity.setText(list[2]);
            holder.total.setText(list[3]);

            return convertView;
        }

    }
}
