
package com.mobile.ict.cart.container;

import com.mobile.ict.cart.util.Master;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONObject;


public class OrderDetails {

    String memberNumber;
    int order_id;
    int group_id;
    String group_name;
    int order_ref_num;
    String timeStamp;
    String status;
    double totalBill;
    int quantity=1;

    public double getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(double totalBill) {
        this.totalBill = totalBill;
    }

    int stockQuantity;

    String stockEnabledStatus;


    TreeMap<String,String[]> placedOrderItemLists;
    TreeMap<String,String[]> processedOrderItemLists;
    HashSet<String> set;


    public OrderDetails(JSONObject order, int pos, String type){

        if(type.equals(Master.PLACEDORDER))
        {

            try {
                this.totalBill=0.0;
                this.order_id=order.getInt("orderId");
                this.timeStamp=order.getString("registeredTime");
                this.stockEnabledStatus=order.getString("stockManagement");
                JSONArray orderItems=order.getJSONArray("orderItems");
                this.placedOrderItemLists =new TreeMap<String, String[]>();
                this.set= new HashSet<>();
                System.out.println("------"+order_id+"-------"+timeStamp+"------"+stockEnabledStatus);
                for(int i=0;i<orderItems.length();i++){
                    JSONObject item=(JSONObject)orderItems.get(i);
                    String name=item.getString("productname");
                    String[] temp=new String[3];
                    temp[0]=item.getString("unitrate");
                    quantity=Integer.parseInt(item.getString("quantity"));
                    if(!set.contains(name))
                    {
                        set.add(name);
                        temp[1]=item.getString("quantity");
                    }
                    else
                    {
                        temp[1]=String.valueOf(Integer.parseInt(this.placedOrderItemLists.get(pos + name)[1]) + Integer.parseInt(item.getString("quantity")));
                    }

                    temp[2]=item.getString("stockquantity");
                    this.placedOrderItemLists.put(pos + name, temp);
                    this.totalBill=this.totalBill+(Double.parseDouble(String.format("%.2f",(Double.parseDouble(temp[0])*quantity))));

                    System.out.println("------" + name + "-------" + temp[0] + "------" + temp[1] + "-----" + temp[2]);


                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else if(type.equals(Master.PROCESSEDORDER))
        {

            try {
                this.totalBill=0.0;
                this.order_id=order.getInt("orderId");
                this.timeStamp=order.getString("registeredTime");
                JSONArray orderItems=order.getJSONArray("orderItems");
                this.processedOrderItemLists=new TreeMap<String, String[]>();
                for(int i=0;i<orderItems.length();i++){
                    JSONObject item=(JSONObject)orderItems.get(i);
                    String name=item.getString("productname");
                    String[] temp=new String[2];
                    temp[0]=item.getString("unitrate");
                    temp[1]=item.getString("quantity");
                    this.totalBill=this.totalBill+(Double.parseDouble(String.format("%.2f",(Double.parseDouble(temp[0])*Integer.parseInt(temp[1])))));
                    this.processedOrderItemLists.put(pos + name, temp);


                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }


    }


    public String getStockEnabledStatus() {
        return stockEnabledStatus;
    }

    public void setStockEnabledStatus(String stockEnabledStatus) {
        this.stockEnabledStatus = stockEnabledStatus;
    }


    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }


    public String getMemberNumber() {
        return memberNumber;
    }

    public void setMemberNumber(String memberNumber) {
        this.memberNumber = memberNumber;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getOrder_ref_num() {
        return order_ref_num;
    }

    public void setOrder_ref_num(int order_ref_num) {
        this.order_ref_num = order_ref_num;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItems() {
        String res="";
        for(String itemName: placedOrderItemLists.keySet()){
            res+= itemName + "-" + placedOrderItemLists.get(itemName)[1] + "\n";
        }
        return res;
    }


    public ArrayList<String[]> getPlacedOrderItemsList(int pos) {

        ArrayList<String[]> itemlist = new ArrayList<>();
        String[] res ;

        int posLen = (""+pos).length();


        for (String itemName: placedOrderItemLists.keySet())
        {

            res = new String[5] ;
            String name =itemName.substring(posLen);
            res[0]=name;
            res[1]= placedOrderItemLists.get(itemName)[0];
            res[2]= placedOrderItemLists.get(itemName)[1];

            double rate=Double.parseDouble(placedOrderItemLists.get(itemName)[0]);
            int quantity=Integer.parseInt(placedOrderItemLists.get(itemName)[1]);
            //  double total = quantity * rate;
            double total = Double.parseDouble(String.format("%.2f",(quantity*rate)));

            res[3]= ""+total;
            res[4]= placedOrderItemLists.get(itemName)[2];

            System.out.println(res[0]+"------"+res[1]+"---------"+res[2]+"--------"+res[3]+"--------"+res[4]);


            itemlist.add(res);
            res=null;
        }

        return itemlist;
    }


    public ArrayList<String[]> getProcessedOrderItemsList(int pos) {

        ArrayList<String[]> itemlist = new ArrayList<>();
        String[] res  ;

        int posLen = (""+pos).length();


        for (String itemName: processedOrderItemLists.keySet())
        {

            res = new String[4] ;
            String name =itemName.substring(posLen);
            res[0]=name;
            res[1]= processedOrderItemLists.get(itemName)[0];
            res[2]= processedOrderItemLists.get(itemName)[1];

            double rate=Double.parseDouble(processedOrderItemLists.get(itemName)[0]);
            int quantity=Integer.parseInt(processedOrderItemLists.get(itemName)[1]);
            //  double total = quantity * rate;
            double total = Double.parseDouble(String.format("%.2f",(quantity*rate)));

            res[3]= ""+total;


            System.out.println(res[0]+"------"+res[1]+"---------"+res[2]+"--------"+res[3]);


            itemlist.add(res);
            res=null;
        }

        return itemlist;
    }


   /* public TreeMap<String,String[]> getPlacedOrderItemLists() {
        return placedOrderItemLists;
    }*/
}
