package com.mobile.ict.cart.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.alertdialogpro.AlertDialogPro;
import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.OrderDetails;
import com.mobile.ict.cart.container.Orders;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.activity.EditPlacedOrderActivity;
import com.mobile.ict.cart.adapter.PlacedOrderAdapter;
import com.mobile.ict.cart.interfaces.EditDeletePlacedOrderInterface;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.Validation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by vish on 21/3/16.
 */
public class PlacedOrderFragment extends Fragment implements EditDeletePlacedOrderInterface {

    View placedOrderFragmentView;
    private SwipeRefreshLayout swipeContainer;
    LinearLayout  emptyCartLinearLayout;
    ArrayList<JSONObject> orderObjects;
    List<Orders> ordersList;
    Orders orders;
    Dialog dialog;
    AlertDialogPro feedbackDialog;
    RecyclerView mRecyclerView;
    PlacedOrderAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    protected LayoutManagerType mCurrentLayoutManagerType;
    RelativeLayout noDataRelativeLayout;
    int optionIndex=-1;
    EditText otherReason;
    String options="",comments,otherReasonText="";
    String stockEnabledStatus="false";

    int pos,orderId,lastpos=0;
    Map<String,String> itemsUnitRateHashMap = new TreeMap<String, String>();
    HashMap<String,Integer> itemsStockQuantityHashMap = new HashMap<>();

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        placedOrderFragmentView = inflater.inflate(R.layout.fragment_placed_order, container, false);
        mRecyclerView = (RecyclerView) placedOrderFragmentView.findViewById(R.id.rvOrder);
        emptyCartLinearLayout = (LinearLayout) placedOrderFragmentView.findViewById(R.id.cartEmptyLinearLayout);
        noDataRelativeLayout = (RelativeLayout) placedOrderFragmentView.findViewById(R.id.noDataRelativeLayout);

        getActivity().setTitle(R.string.title_fragment_placed_order);

        swipeContainer = (SwipeRefreshLayout) placedOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new FetchOrders(false).execute();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);
      //  new FetchOrders().execute();
        return placedOrderFragmentView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*new FetchOrders(true).execute();

        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }



        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);*/

        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchOrders(true).execute();

        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
    }

    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType)
        {
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;

            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }




    public List<Orders> getOrders() {

        ordersList = new ArrayList<>();

        int count=0;


        if(orderObjects.size()!=0)

        {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyCartLinearLayout.setVisibility(View.GONE);


            for (JSONObject entry : orderObjects) {
                  OrderDetails orderDetails = new OrderDetails(entry, count,Master.PLACEDORDER);

                orders = new Orders(orderDetails, Arrays.asList(orderDetails.getPlacedOrderItemsList(count)));
                ordersList.add(orders);
                count++;
            }
        }
        else
        {

            emptyCartLinearLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);

           /* AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(R.string.alert_No_Placed_Orders_Found)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.dismiss();
                        }
                    });

            dialog = builder.show();*/
        }

        return ordersList;
    }


    @Override
    public void deletePlacedOrder()
   // public void deletePlacedOrder(int orderId,int position)
    {
        //orderID=orderId;

        System.out.println("deleting order------"+"pos------------"+pos+"-----------orderId-------"+orderId);
       // pos=position;
        openFeedBackDialog(orderId);
    }

    @Override
    public void editPlacedOrder() {
        System.out.println("editing orderDetails------" + "pos------------" + pos + "-----------orderId-------" + orderId);

        itemsUnitRateHashMap.clear();
        List<?> list_= ordersList.get(pos).getChildItemList();
        ArrayList<String[]> list = (ArrayList)list_.get(0);
        OrderDetails orderDetails = ordersList.get(pos).getOrderDetails();
        stockEnabledStatus= orderDetails.getStockEnabledStatus();
        for(int j=0;j<list.size();j++)
        {

            itemsUnitRateHashMap.put(list.get(j)[0], list.get(j)[1]);
            itemsStockQuantityHashMap.put(list.get(j)[0], Integer.parseInt(list.get(j)[4]));

        }

        Intent i=new Intent(getActivity(),EditPlacedOrderActivity.class);
        i.putExtra("itemsUnitRateHashMap", (Serializable) itemsUnitRateHashMap);
        i.putExtra("itemsStockQuantityHashMap",(Serializable) itemsStockQuantityHashMap);
        i.putExtra("stockEnabledStatus",stockEnabledStatus);
        i.putExtra("orderId", orderDetails.getOrder_id());
        for(int j=0;j<list.size();j++)
        {
            i.putExtra(list.get(j)[0], Integer.parseInt(list.get(j)[2]));
        }
        startActivity(i);
    }


    public void openFeedBackDialog( final int orderId)
    {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View Layout = inflater.inflate(R.layout.cancel_placed_order_feedback, (ViewGroup)placedOrderFragmentView. findViewById(R.id.cancel_order_feedback), false);
        final AlertDialogPro.Builder builder = new AlertDialogPro.Builder(getActivity());
        builder.setView(Layout);
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        feedbackDialog = builder.create();

        feedbackDialog.show();

        final ListView lv = (ListView)Layout.findViewById(R.id.lv1);
        lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        otherReason =(EditText)Layout.findViewById(R.id.comments);
        otherReason.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if(!s.equals(""))
                    otherReasonText=s.toString();
                else
                    otherReasonText="";
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        final ArrayList<HashMap<String, Object>> m_data = new ArrayList<HashMap<String, Object>>();

        HashMap<String, Object> map1 = new HashMap<String, Object>();
        map1.put("option",getResources().getString(R.string.label_cancelorder_feedback_form_option1));
        m_data.add(map1);

        HashMap<String, Object> map2 = new HashMap<String, Object>();
        map2.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option2));// no small text of this item!
        m_data.add(map2);

        HashMap<String, Object> map3 = new HashMap<String, Object>();
        map3.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option3));
        m_data.add(map3);

        HashMap<String, Object> map4 = new HashMap<String, Object>();
        map4.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option4));
        m_data.add(map4);

        HashMap<String, Object> map5 = new HashMap<String, Object>();
        map5.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option5));
        m_data.add(map5);

        for (HashMap<String, Object> m :m_data)
            m.put("checked", false);

        final SimpleAdapter adapter = new SimpleAdapter(getActivity(),
                m_data,
                R.layout.cancel_placed_order_feedback_item,
                new String[] {"option","checked"},
                new int[] {R.id.options, R.id.rb_choice});

        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            public boolean setViewValue(View view, Object data, String textRepresentation) {

                view.setVisibility(View.VISIBLE);
                return false;
            }

        });


        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
                RadioButton rb = (RadioButton) v.findViewById(R.id.rb_choice);

                if (m_data.get(arg2).get("option").equals(getResources().getString(R.string.label_cancelorder_feedback_form_option5))) {
                    otherReason.setVisibility(View.VISIBLE);
                } else {
                    if (otherReason.isShown()) {
                        otherReason.setVisibility(View.GONE);
                        otherReason.setError(null);

                    }

                }

                options = m_data.get(arg2).get("option").toString();

                optionIndex = arg2;

                if (!rb.isChecked()) {
                    for (HashMap<String, Object> m : m_data)
                        m.put("checked", false);
                    m_data.get(arg2).put("checked", true);
                    adapter.notifyDataSetChanged();
                }
            }
        });





        feedbackDialog.getButton(AlertDialogPro.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (options.equals("")) {
                    Toast.makeText(getActivity(),R.string.label_toast_cancel_order_feedback_form, Toast.LENGTH_LONG).show();

                } else {
                    if(options.equals(getResources().getString(R.string.label_cancelorder_feedback_form_option5)))
                    {
                        if(!otherReason.getText().toString().equals(""))
                        {

                            comments=otherReason.getText().toString().replaceAll("\\n", " ");
                            otherReasonText=comments;
                            feedbackDialog.dismiss();
                            new CancelOrder().execute(String.valueOf(orderId));



                        }
                        else
                        {
                            Validation.hasText(getActivity(), otherReason);

                        }
                    }
                    else
                    {
                        comments=options;

                        feedbackDialog.dismiss();
                        new CancelOrder().execute(String.valueOf(orderId));



                    }
                }

                options="";


            }
        });

        feedbackDialog.getButton(AlertDialogPro.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherReasonText = "";
                feedbackDialog.dismiss();
            }
        });


    }


    public class FetchOrders extends AsyncTask<String,String, String>
    {

        Boolean showProgressDialog;

        public FetchOrders(Boolean showProgressDialog) {
            this.showProgressDialog = showProgressDialog;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(showProgressDialog)
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_fetching_orders), false);

        }

        @Override
        protected String doInBackground(String... params) {

                Master.getJSON = new GetJSON();

            System.out.println("--------placed order fragment-------------------"+MemberDetails.getSelectedOrgAbbr());

             // String url = "http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/search/getOrdersForMember?format=binary&status=saved&abbr=Test2&phonenumber=919827755129&projection=default";
            //  System.out.println("URL: " + url);
              Master.response = Master.getJSON.getJSONFromUrl(Master.getPlacedOrderURL(MemberDetails.getSelectedOrgAbbr(),MemberDetails.getMobileNumber()), null, "GET", true,MemberDetails.getEmail(),MemberDetails.getPassword());
              System.out.println(Master.response);
              return Master.response;

        }

        @Override
        protected void onPostExecute(String response) {

            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(PlacedOrderFragment.this.isAdded())
            {
                if(response.equals("exception"))
                {
                    //Material.alertDialog(getActivity(), getString(R.string.alert_cannot_connect_to_the_server), "OK");
                    mRecyclerView.setVisibility(View.GONE);
                    emptyCartLinearLayout.setVisibility(View.GONE);
                    noDataRelativeLayout.setVisibility(View.VISIBLE);
                }
                else
                {
                    try {


                        JSONObject orderList = new JSONObject(response);
                        JSONArray ordersArray = orderList.getJSONArray("orders");
                        orderObjects=new ArrayList<JSONObject>();

                        for(int i=0;i<ordersArray.length();i++)
                        {
                            orderObjects.add((JSONObject)ordersArray.get(i));
                        }


                        ordersList = getOrders();
                        mAdapter= new PlacedOrderAdapter(getActivity(), ordersList,PlacedOrderFragment.this,Master.PLACEDORDER);
                        mRecyclerView.setHasFixedSize(true);
                        mRecyclerView.setAdapter(mAdapter);

                        mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {

                            @Override
                            public void onListItemExpanded(int position) {

                                List<? extends ParentListItem> parentItemList = mAdapter.getParentItemList();
                                if(lastpos!=position)
                                {
                                    mAdapter.collapseParent(lastpos);
                                }
                                mRecyclerView.smoothScrollToPosition(position + parentItemList.get(position).getChildItemList().size());
                                lastpos=position;
                                pos=position;
                                orderId= ordersList.get(position).getOrderDetails().getOrder_id();
                            }

                            @Override
                            public void onListItemCollapsed(int position) {

                            }
                        });


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        // e.printStackTrace();
                        //Material.alertDialog(getActivity(), getString(R.string.alert_No_Placed_Orders_Found), "OK");
                        mRecyclerView.setVisibility(View.GONE);
                        emptyCartLinearLayout.setVisibility(View.VISIBLE);

                    }


                    swipeContainer.setRefreshing(false);

                }

            }

        }



    }



    public class CancelOrder extends AsyncTask<String, String, String>{


        String val;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Material.circularProgressDialog(getActivity(), getString(R.string.pd_cancel_orders), false);
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub


           // String url="http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/update/"+params[0];


            JSONObject obj = new JSONObject();
            try {
                obj.put("status","cancelled");
                obj.put("comments","");
            } catch (JSONException e) {
                e.printStackTrace();
            }
           // System.out.println("URL: " + url);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getCancellingOrderURL(params[0]),obj, "POST", true, MemberDetails.getEmail(),MemberDetails.getPassword());
            System.out.println(Master.response);
            return Master.response;





        }

        protected void onPostExecute(String response1) {

            System.out.println("response-------------------------"+response1);

            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(PlacedOrderFragment.this.isAdded())
            {
                if(response1.equals("exception"))
                {
                    Material.alertDialog(getActivity(), getString(R.string.alert_cannot_connect_to_the_server), "OK");
                }
                else {
                    JSONObject jsonObj = null;
                    try {
                        jsonObj = new JSONObject(response1);
                        val=jsonObj.getString("status");

                        if(val.equals("Success"))
                        {

                            Toast.makeText(getActivity(),R.string.label_toast_Your_order_has_been_cancelled_successfully, Toast.LENGTH_LONG).show();

                            System.out.println("removing pos-------------" + pos);
                            mAdapter.collapseParent(pos);
                            ordersList.remove(pos);
                            mAdapter.notifyParentItemRemoved(pos);
                            mAdapter.notifyDataSetChanged();


                            if(ordersList.isEmpty())
                            {
                                emptyCartLinearLayout.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            }


                        }

                        else

                            Toast.makeText(getActivity(),R.string.label_toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }



        }

    }




}
