package com.mobile.ict.cart.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.OrderDetails;
import com.mobile.ict.cart.container.Orders;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.adapter.PlacedOrderAdapter;
import com.mobile.ict.cart.interfaces.EditDeletePlacedOrderInterface;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vish on 21/3/16.
 */
public class ProcessedOrderFragment extends Fragment implements EditDeletePlacedOrderInterface{

    View processedOrderFragmentView;
    private SwipeRefreshLayout swipeContainer;
    ArrayList<JSONObject> orderObjects;
    List<Orders> ordersList;
    Orders orders;
    RecyclerView mRecyclerView;
    PlacedOrderAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    protected LayoutManagerType mCurrentLayoutManagerType;
    int pos,orderId,lastpos=0;
    LinearLayout emptyCartLinearLayout;
    RelativeLayout noDataRelativeLayout;


    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        processedOrderFragmentView = inflater.inflate(R.layout.fragment_processed_order, container, false);
        mRecyclerView = (RecyclerView) processedOrderFragmentView.findViewById(R.id.rvOrder);
        emptyCartLinearLayout = (LinearLayout) processedOrderFragmentView.findViewById(R.id.cartEmptyLinearLayout);
        noDataRelativeLayout = (RelativeLayout) processedOrderFragmentView.findViewById(R.id.noDataRelativeLayout);

        getActivity().setTitle(R.string.title_fragment_processed_order);
        swipeContainer = (SwipeRefreshLayout) processedOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new FetchOrders(false).execute();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);

       // new FetchOrders().execute();
        return processedOrderFragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new FetchOrders(true).execute();
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }



        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
    }


    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType)
        {
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;

            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }




    public List<Orders> getOrders() {

        ordersList = new ArrayList<>();

        int count=0;


        if(orderObjects.size()!=0)

        {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyCartLinearLayout.setVisibility(View.GONE);
            for (JSONObject entry : orderObjects) {
                OrderDetails orderDetails = new OrderDetails(entry, count,Master.PROCESSEDORDER);
                orders = new Orders(orderDetails, Arrays.asList(orderDetails.getProcessedOrderItemsList(count)));
                ordersList.add(orders);
                count++;
            }
        }
        else
        {

            mRecyclerView.setVisibility(View.GONE);
            emptyCartLinearLayout.setVisibility(View.VISIBLE);

           /* AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(R.string.alert_No_Processed_Orders_Found)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.dismiss();
                        }
                    });

            dialog = builder.show();*/
        }

        return ordersList;
    }


    @Override
    public void deletePlacedOrder()

    {

    }

    @Override
    public void editPlacedOrder() {

    }


    public class FetchOrders extends AsyncTask<String,String, String>
    {
        Boolean showProgressDialog;

        public FetchOrders(Boolean showProgressDialog) {
            this.showProgressDialog = showProgressDialog;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(showProgressDialog)
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_fetching_orders), false);

        }

        @Override
        protected String doInBackground(String... params) {



            Master.getJSON = new GetJSON();

           // String url=  "http://ruralict.cse.iitb.ac.in/ruralict/api/orders/search/getOrdersForMember?format=binary&status=processed&abbr=Test2&phonenumber=919827755129&projection=default";
           // System.out.println("URL: " + url);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getProcessedOrderURL(MemberDetails.getSelectedOrgAbbr(),MemberDetails.getMobileNumber()), null, "GET", true, MemberDetails.getEmail(), MemberDetails.getPassword());
            System.out.println(Master.response);
            return Master.response;

        }

        @Override
        protected void onPostExecute(String response) {

            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(ProcessedOrderFragment.this.isAdded())
            {
                if(response.equals("exception"))
                {
                    //Material.alertDialog(getActivity(), getString(R.string.alert_cannot_connect_to_the_server), "OK");
                    mRecyclerView.setVisibility(View.GONE);
                    emptyCartLinearLayout.setVisibility(View.GONE);
                    noDataRelativeLayout.setVisibility(View.VISIBLE);
                }
                else
                {
                    try {


                        JSONObject orderList = new JSONObject(response);
                        JSONArray ordersArray = orderList.getJSONArray("orders");
                        orderObjects=new ArrayList<JSONObject>();

                        for(int i=0;i<ordersArray.length();i++)
                        {
                            orderObjects.add((JSONObject)ordersArray.get(i));

                        }


                        ordersList = getOrders();
                        mAdapter= new PlacedOrderAdapter(getActivity(), ordersList,ProcessedOrderFragment.this,Master.PROCESSEDORDER);
                        mRecyclerView.setHasFixedSize(true);
                        mRecyclerView.setAdapter(mAdapter);

                        mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
                            List<? extends ParentListItem> parentItemList = mAdapter.getParentItemList();

                            @Override
                            public void onListItemExpanded(int position) {

                                if(lastpos!=position)
                                {
                                    mAdapter.collapseParent(lastpos);
                                }
                                mRecyclerView.smoothScrollToPosition(position + parentItemList.get(position).getChildItemList().size());

                                lastpos=position;
                                pos=position;
                                orderId= ordersList.get(position).getOrderDetails().getOrder_id();
                            }

                            @Override
                            public void onListItemCollapsed(int position) {

                            }
                        });


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        // e.printStackTrace();
                        // Material.alertDialog(getActivity(), getString(R.string.alert_No_Processed_Orders_Found), "OK");

                        mRecyclerView.setVisibility(View.GONE);
                        emptyCartLinearLayout.setVisibility(View.VISIBLE);
                    }


                    swipeContainer.setRefreshing(false);


                }
            }


        }



    }






}
