package com.mobile.ict.cart.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Organisations;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.JSONDataHelper;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.SharedPreferenceConnector;
import com.mobile.ict.cart.util.Validation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Siddharthsingh on 06-05-2016.
 */

public class ReferralFragment extends Fragment {

    EditText phoneNumberNew;
    EditText emailNew;
    Spinner spinner;
    Button refer;
    ArrayAdapter<String> adapter;
    List<String> list;
    int position = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_referral, container, false);

        getActivity().setTitle(R.string.title_fragment_referral);

        phoneNumberNew = (EditText) view.findViewById(R.id.phonenumber_et);
        emailNew = (EditText) view.findViewById(R.id.email_et);
        refer = (Button) view.findViewById(R.id.refer_button);
        spinner = (Spinner) view.findViewById(R.id.referra_org_spinner);

        list = new ArrayList<String>();
        list.add(getActivity().getString(R.string.label_loading_org_list));


        adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, list);
        spinner.setAdapter(adapter);


        if (Master.isNetworkAvailable(getActivity())) {

            try {
                JSONObject obj = new JSONObject();
                obj.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());
                obj.put(Master.PASSWORD, MemberDetails.getPassword());
                new GetOrganisationsTask().execute(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

            Toast.makeText(getActivity(), R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
            refer.setEnabled(false);

        }


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                position = pos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //check if the feedbcak edit text has any text
                if (phoneNumberNew.getText().toString().trim().equals("")
                        || emailNew.getText().toString().trim().equals("")) {
                    Toast.makeText(getActivity(), R.string.toast_please_fill_all_the_details, Toast.LENGTH_LONG).show();
                } else if (phoneNumberNew.getText().toString().trim().length() != 10) {
                    Toast.makeText(getActivity(), R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();
                } else if (!Validation.isValidEmail(emailNew.getText().toString().trim())) {
                    Toast.makeText(getActivity(), R.string.toast_enter_valid_emailid, Toast.LENGTH_LONG).show();
                } else {
                    String Orgabbr;

                    //chech if select organisation is selected in spinner
                    if (position != 0) {
                        Orgabbr = Organisations.organisationList.get(position - 1).getOrgabbr();
                        String phoneNumber = "91" + phoneNumberNew.getText().toString();
                        String email = emailNew.getText().toString();

                        try {
                            JSONObject obj = new JSONObject();
                            obj.put("email", email);
                            obj.put(Master.DEFAULT_ORG_ABBR, Orgabbr);
                            obj.put("phonenumber", phoneNumber);
                            obj.put("refemail", MemberDetails.email);

                            new SendReferralTask().execute(obj);
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    } else {
                        //show message to choose organisation
                        Toast.makeText(getActivity(), R.string.toast_please_select_an_organisation, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }


    public class SendReferralTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), "Sending Referral", false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            System.out.println(params[0]);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getSendReferURL(), params[0], "POST", true,
                    MemberDetails.getEmail(), MemberDetails.getPassword());
            System.out.println("Send Feedback Response " + Master.response);
            Log.d("Send referral Response ", Master.response);
            //can't call Toast as it displays on UI from background thread
            // Toast.makeText(getContext(),Master.response,Toast.LENGTH_SHORT).show();
            return Master.response;

        }

        @Override
        protected void onPostExecute(String response) {

            if (Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            System.out.println("------------" + response);
            if (response.equals("exception")) {
                Toast.makeText(getActivity(), R.string.toast_error_while_sending_referral, Toast.LENGTH_LONG).show();
            } else {
                try {
                    JSONObject responseJson = new JSONObject(response);
                    if (responseJson.has("response")) {
                        String resp = responseJson.getString("response");
                        switch (resp.toLowerCase()) {
                            case "Already a member": {
                                Toast.makeText(getActivity(), R.string.toast_already_a_member, Toast.LENGTH_SHORT).show();
                                break;
                            }

                            case "success": {
                                Toast.makeText(getActivity(), R.string.toast_member_referred_successfully, Toast.LENGTH_SHORT).show();
                                break;
                            }

                            case "failure": {

                                if (responseJson.has("reason")) {
                                    Toast.makeText(getActivity(), R.string.toast_error_cannot_refer_admin, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), R.string.toast_error_refer, Toast.LENGTH_SHORT).show();
                                }

                                break;
                            }


                        }

                        SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, response);
                    }


                } catch (JSONException e) {
                    Toast.makeText(getActivity(), R.string.toast_error_refer, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        }
    }


    public class GetOrganisationsTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_fetching_data_from_server), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            GetJSON getJson = new GetJSON();
            System.out.println(params[0]);
            Master.response = getJson.getJSONFromUrl(Master.getLoginURL(), params[0], "POST", false, null, null);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response) {

            if (Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            System.out.println("------------" + response);
            if (response.equals("exception")) {
                //clear all previous values in the list and show the error
                list.clear();
                list.add(getActivity().getString(R.string.label_error_loading_organisations));
                adapter.notifyDataSetChanged();
            } else {
                SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, response);
                Organisations.organisationList = new ArrayList<Organisations>();
                Organisations.organisationList = JSONDataHelper.getOrganisationListFromJson(getActivity(), SharedPreferenceConnector
                        .readString(getActivity().getApplicationContext(), Master.LOGIN_JSON, Master.DEFAULT_LOGIN_JSON));
                list.clear();

                if (Organisations.organisationList.size() > 0) {
                    list.add(getActivity().getString(R.string.label_select_organisation));
                    for (int i = 0; i < Organisations.organisationList.size(); ++i) {
                        list.add(Organisations.organisationList.get(i).getName());
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    list.add(getActivity().getString(R.string.label_error_loading_organisations));
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }
}
