package com.mobile.ict.cart.interfaces;

/**
 * Created by root on 3/5/16.
 */
public interface EditDeletePlacedOrderInterface {

    //public void deletePlacedOrder(int orderId,int pos);
    public void deletePlacedOrder();
    public void editPlacedOrder();
}
